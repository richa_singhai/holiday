/**
 * @author: Anil Kumar Ch
 * @Date: 24-06-2021
 */
var express = require('express');
var router = express.Router();
var userRouter = require('./user');

router.use('/v1/user', userRouter);

/**
 * @description: unknown Router call
 * @author: Anil Kumar Ch 
 * @date :24-06-2021
 */
 router.get('*', function (req, res) {
	res.send('Hello Backend API Route');
});


module.exports = router;
