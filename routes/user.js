/**
 * @author: Anil Kumar Ch
 * @Date: 24-06-2021
 */
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/login', function(req, res, next) {
  res.send('Login Succ');
});

/**
 * @description: unknown Router call
 * @author: Anil Kumar Ch 
 * @date :24-06-2021
 */
 router.get('*', function (req, res) {
	res.send('Hello DB USER Route');
});

module.exports = router;
